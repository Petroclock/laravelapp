<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div class="container">
<form action="{{action('Controller@get')}}" method="post">
    <div class="form-row">
        <div class="form-group">
            <label for="name">Название</label>
            <input name="name" type="text" class="form-control" id="name" placeholder="">
        </div>
        <div class="form-group col-md-2">
            <label for="amount-of">сумма От</label>
            <select name="amount-of" id="amount-of" class="form-control">
                @foreach ($amount as $item)
                    <option>{{ $item }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-2">
            <label for="amount-up"> сумма До</label>
            <select name="amount-up" id="amount-up" class="form-control">
                {{$last = array_pop($amount)}}
                @foreach ($amount as $item)
                    <option>{{ $item }}</option>
                @endforeach
                <option selected>{{ $last }}</option>
            </select>
        </div>
    </div>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<table class="table">
    <thead>
    <tr>
        <th scope="col">Начало</th>
        <th scope="col">Название</th>
        <th scope="col">Билет</th>
        <th scope="col">Возраст</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($schedule as $item)
        <tr>
            <td>{{ $item->time_at }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->price }} р.</td>
            <td>{{ $item->age }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>
</body>
</html>
