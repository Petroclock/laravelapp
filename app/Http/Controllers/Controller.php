<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
# require_once 'vendor/electrolinux/phpquery/phpQuery/phpQuery.php';
use phpQuery;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function get(Request $request)
    {
        $schedule = DB::table('schedule');
        if ($request->has(['amount-of', 'amount-up'])) {
            $amount_of = $request->input('amount-of');
            $amount_up = $request->input('amount-up');
            $schedule = $schedule->whereBetween('price', [$amount_of, $amount_up]);
        }
        if ($request->has(['name'])) {
            $name = $request->input('name');
            $schedule = $schedule->where('name', '=', $name);
        }
        $amount = DB::table('schedule')->distinct()->orderBy('price', 'asc')->pluck('price');
        return view('get', ['schedule' => $schedule->get(), 'amount' => $amount->all()]);
    }

    public function update()
    {

        $rows = $this->parse();
        DB::table('schedule')->truncate();
        foreach ($rows as $row){
            DB::table('schedule')->insert(
                ['name' => $row[1], 'time_at' => $row[0], 'price' => $row[3], 'age' => $row[2]]
            );
        }

        return view('update');
    }

    public function parse(){

        $handle = fopen("https://cityopen.ru/afisha/kinoteatr-salavat/", "rb");
        $contents = stream_get_contents($handle);
        fclose($handle);

        $dom = phpQuery::newDocument($contents);
        $row = 0;
        $res = [];
        foreach($dom->find("tr") as $key => $value){
            $pq = pq($value);
            if ($pq->find('strong')->length > 0){
                continue;
            }
            $list = $pq->find('td');

            $col = 0;

            foreach ($list as $item) {
                $res[$row][$col] = pq($item)->text();
                $col ++;
            }
            $row ++;
        }

        phpQuery::unloadDocuments();
        return $res;
    }
}
